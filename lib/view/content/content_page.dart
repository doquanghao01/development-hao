import 'package:development/controllers/content_controllers.dart';
import 'package:development/view/content/widgets/content_head.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'widgets/content_popular.dart';
import 'widgets/content_recent.dart';

class ContentPage extends StatelessWidget {
  const ContentPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<ContentController>(
        builder: (controller) => Container(
          padding: const EdgeInsets.only(top: 70),
          color: const Color(0xFFc5e5f3),
          child: Column(
            children: [
              const ContentHead(),
              Expanded(
                child: ContentPopular(
                  list: controller.listDetail,
                ),
              ),
              Expanded(
                child: ContentRecent(
                  list: controller.listRecent,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
