import 'package:development/view/widgets/text_content.dart';
import 'package:development/view/widgets/text_title.dart';
import 'package:flutter/material.dart';

class ContentTitleBar extends StatelessWidget {
  const ContentTitleBar({
    Key? key,
    required this.text,
    required this.press,
  }) : super(key: key);
  final String text;
  final VoidCallback press;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 25, right: 25),
      child: Row(
        children: [
          TextTitle(
            text: text,
            color: const Color(0xFF1f2326),
            size: 20,
          ),
          const Spacer(
            flex: 2,
          ),
          const TextContent(
            text: "Show all",
            color: Colors.amber,
            size: 15,
          ),
          const SizedBox(
            width: 10,
          ),
          Container(
            width: 50,
            height: 50,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: const Color(0xFFfdc33c)),
            child: GestureDetector(
              onTap: press,
            ),
          )
        ],
      ),
    );
  }
}
