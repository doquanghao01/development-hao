import 'package:development/view/widgets/text_content.dart';
import 'package:development/view/widgets/text_title.dart';
import 'package:flutter/material.dart';

class ContentHead extends StatelessWidget {
  const ContentHead({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 100,
      margin: const EdgeInsets.only(left: 25, right: 25),
      padding: const EdgeInsets.only(left: 20, right: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: const Color(0xFFebf8fd),
      ),
      child: Row(
        children: [
          const CircleAvatar(
            radius: 40,
            backgroundImage: AssetImage("img/background.jpg"),
          ),
          const SizedBox(
            width: 10,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              TextTitle(
                text: "James Smith",
                color: Color(0xFF3b3f42),
                size: 18,
              ),
              SizedBox(
                height: 5,
              ),
              TextContent(
                text: "Top Level",
                size: 12,
                color: Colors.amber,
              ),
            ],
          ),
          Expanded(child: Container()),
          Container(
            width: 70,
            height: 70,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
            ),
            child: const Center(
              child: Icon(
                Icons.notifications,
                color: Color(0xFF69c5df),
                size: 30,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
