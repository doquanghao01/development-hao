import 'package:development/view/widgets/app_card_popular.dart';
import 'package:flutter/material.dart';

import 'content_titlebar.dart';

class ContentPopular extends StatelessWidget {
  const ContentPopular({
    Key? key,
    required this.list,
  }) : super(key: key);
  final List list;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 30,
        ),
        //popular contest
        ContentTitleBar(
          press: () {},
          text: 'Popular Contest',
        ),
        const SizedBox(height: 20),
        Expanded(
          child: PageView.builder(
            controller: PageController(viewportFraction: 0.88),
            itemCount: list.length,
            itemBuilder: (_, i) {
              return AppCardPopular(
                list: list,
                i: i,
              );
            },
          ),
        ),
      ],
    );
  }
}
