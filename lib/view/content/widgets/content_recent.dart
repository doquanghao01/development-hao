import 'package:development/view/widgets/text_content.dart';
import 'package:flutter/material.dart';

import 'content_titlebar.dart';

class ContentRecent extends StatelessWidget {
  const ContentRecent({
    Key? key,
    required this.list,
  }) : super(key: key);
  final List list;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 30),
        ContentTitleBar(
          press: () {
            //  Get.to(() => const RecentContest());
          },
          text: 'Recent Contests',
        ),
        const SizedBox(height: 20),
        Expanded(
          child: MediaQuery.removePadding(
            context: context,
            removeTop: true,
            child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              itemCount: list.length,
              itemBuilder: (_, i) {
                return Container(
                  width: MediaQuery.of(context).size.width,
                  height: 100,
                  margin:
                      const EdgeInsets.only(left: 25, right: 25, bottom: 20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: const Color(0xFFebf8fd),
                  ),
                  child: Container(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: Row(
                      children: [
                        CircleAvatar(
                          radius: 40,
                          backgroundImage: AssetImage(list[i]['img']),
                        ),
                        const SizedBox(width: 10),
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              TextContent(
                                text: list[i]['status'],
                                size: 18,
                                color: Colors.amber,
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              TextContent(
                                text: list[i]['text'],
                                size: 18,
                                color: const Color(0xFF3b3f42),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(width: 10),
                        SizedBox(
                          width: 70,
                          height: 70,
                          child: TextContent(
                            text: list[i]['time'],
                            size: 14,
                            color: const Color(0xFFb2b8bb),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}
