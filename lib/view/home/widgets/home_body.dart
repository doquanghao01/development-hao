import 'package:development/route/screen_bindings.dart';
import 'package:development/view/content/content_page.dart';
import 'package:development/view/widgets/app_button.dart';
import 'package:development/view/widgets/text_content.dart';
import 'package:development/view/widgets/text_title.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'home_imgbackgroud.dart';

class HomeBody extends StatelessWidget {
  const HomeBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const ImgBackgroud(),
        const TextTitle(
          text: 'Pick Your Favourite',
        ),
        const TextTitle(
          text: 'Contests',
        ),
        const Spacer(
          flex: 1,
        ),
        SizedBox(
          width: MediaQuery.of(context).size.width - 25,
          child: const TextContent(
            text: "We make great design work "
                "happen with our great community designer",
            color: Colors.white60,
            size: 18,
          ),
        ),
        const Spacer(
          flex: 1,
        ),
        AppButton(
          text: "Get started",
          press: () =>
              Get.to(() => const ContentPage(), binding: ScreenBindings()),
        ),
        const Spacer(
          flex: 2,
        ),
      ],
    );
  }
}
