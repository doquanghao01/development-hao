import 'package:flutter/material.dart';

class ImgBackgroud extends StatelessWidget {
  const ImgBackgroud({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 1.5,
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration(
        image: DecorationImage(
            image: AssetImage("img/background.jpg"), fit: BoxFit.cover),
      ),
    );
  }
}
