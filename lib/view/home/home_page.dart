import 'package:flutter/material.dart';

import 'widgets/home_body.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFF69c5df),
      body: Container(
        padding: const EdgeInsets.only(left: 20),
        child: const HomeBody(),
      ),
    );
  }
}
