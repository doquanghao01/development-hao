import 'package:development/view/widgets/text_content.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DetailHead extends StatelessWidget {
  const DetailHead({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 120,
      left: 0,
      height: 100,
      width: MediaQuery.of(context).size.width,
      child: Container(
        margin: const EdgeInsets.only(left: 25, right: 25),
        padding: const EdgeInsets.only(left: 20, right: 20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: const Color(0xFFebf8fd),
        ),
        child: Row(
          children: [
            CircleAvatar(
              radius: 40,
              backgroundImage: AssetImage(Get.arguments['tile']['img']),
            ),
            const SizedBox(
              width: 10,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextContent(
                  text: Get.arguments['tile']['name'],
                  size: 18,
                  color: const Color(0xFF3b3f42),
                ),
                const SizedBox(
                  height: 5,
                ),
                const TextContent(
                  text: "Top Level",
                  size: 12,
                  color: Colors.amber,
                ),
              ],
            ),
            const Spacer(),
            Container(
              width: 70,
              height: 70,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: const Color(0xFFf3fafc)),
              child: const Center(
                child: Icon(
                  Icons.notifications,
                  color: Color(0xFF69c5df),
                  size: 30,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
