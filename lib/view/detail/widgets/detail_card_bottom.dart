import 'package:development/view/widgets/text_content.dart';
import 'package:development/view/widgets/text_title.dart';
import 'package:flutter/material.dart';

class DetailCardBottom extends StatelessWidget {
  const DetailCardBottom({
    Key? key,
    required this.textTitle,
    required this.textContent,
    required this.iconData,
  }) : super(key: key);
  final String textTitle;
  final String textContent;
  final IconData iconData;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Icon(iconData, color: const Color(0xFF69c5df)),
        const SizedBox(
          width: 5,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextTitle(
              text: textTitle,
              size: 18,
              color: const Color(0xFF303030),
            ),
            TextContent(
              text: textContent,
              size: 18,
              color: const Color(0xFFacacac),
            )
          ],
        )
      ],
    );
  }
}
