import 'package:development/controllers/detail_controller.dart';
import 'package:development/view/widgets/text_content.dart';
import 'package:development/view/widgets/text_title.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'detail_add_to_favorite.dart';
import 'detail_card_content.dart';
import 'detail_head.dart';
import 'detail_total_participants.dart';

class DetailBody extends StatelessWidget {
  const DetailBody({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<DetailController>(
      builder: (controller) => Stack(
        children: [
          Positioned(
            top: 50,
            left: 10,
            child: IconButton(
              onPressed: () => Get.back(),
              icon: const Icon(Icons.arrow_back_ios),
            ),
          ),
          const DetailHead(),
          Positioned(
            top: 320,
            left: 0,
            width: Get.width,
            height: Get.height,
            child: Container(
              width: 80,
              height: 80,
              color: const Color(0xFFf9fbfc),
            ),
          ),
          const DetailCardContent(),
          Positioned(
            top: 540,
            left: 25,
            height: 50,
            child: Column(
              children: [
                Row(
                  children: const [
                    TextTitle(
                      text: "Total Participants ",
                      size: 20,
                      color: Colors.black,
                    ),
                    TextContent(
                      text: "(11)",
                      size: 18,
                      color: Color(0xFFfbc33e),
                    )
                  ],
                ),
              ],
            ),
          ),
          DetailTotalParticipants(
            list: controller.listImg,
          ),
          const DetailAddToFavorite()
        ],
      ),
    );
  }
}
