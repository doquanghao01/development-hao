import 'package:development/view/widgets/text_content.dart';
import 'package:flutter/material.dart';

class DetailAddToFavorite extends StatelessWidget {
  const DetailAddToFavorite({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 670,
      left: 25,
      child: Row(
        children: [
          Container(
              width: 60,
              height: 60,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: const Color(0xFFfbc33e)),
              child: const Icon(Icons.favorite_border, color: Colors.white)),
          const SizedBox(
            width: 10,
          ),
          const TextContent(
            text: "Add to favorite",
            color: Color(0xFFfbc33e),
            size: 18,
          )
        ],
      ),
    );
  }
}
