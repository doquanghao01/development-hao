import 'package:development/view/detail/widgets/detail_card_bottom.dart';
import 'package:development/view/widgets/text_content.dart';
import 'package:development/view/widgets/text_title.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DetailCardContent extends StatelessWidget {
  const DetailCardContent({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 250,
      left: 0,
      width: MediaQuery.of(context).size.width,
      child: Container(
        margin: const EdgeInsets.only(left: 25, right: 25),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: const Color(0xFFfcfffe),
            boxShadow: [
              BoxShadow(
                  blurRadius: 20,
                  spreadRadius: 1,
                  offset: const Offset(0, 10),
                  color: Colors.grey.withOpacity(0.2))
            ]),
        child: Container(
          margin:
              const EdgeInsets.only(left: 20, top: 20, bottom: 20, right: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextTitle(
                text: Get.arguments['tile']['title'],
                size: 30,
                color: Colors.black,
              ),
              const SizedBox(height: 20),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: TextContent(
                  text: Get.arguments['tile']['text'],
                  size: 20,
                  color: const Color(0xFFb8b8b8),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              const Divider(
                thickness: 1.0,
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  DetailCardBottom(
                    iconData: Icons.watch_later,
                    textTitle: Get.arguments['tile']['time'],
                    textContent: "Deadine",
                  ),
                  DetailCardBottom(
                    iconData: Icons.monetization_on,
                    textTitle: Get.arguments['tile']['prize'],
                    textContent: "Prize",
                  ),
                  const DetailCardBottom(
                    iconData: Icons.star,
                    textTitle: "Top Level",
                    textContent: "Entry",
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
