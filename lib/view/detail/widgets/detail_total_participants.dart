import 'package:flutter/material.dart';

class DetailTotalParticipants extends StatelessWidget {
  const DetailTotalParticipants({
    Key? key,
    required this.list,
  }) : super(key: key);
  final List list;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        for (int i = 0; i < list.length; i++)
          Positioned(
            top: 590,
            left: (20 + i * 35).toDouble(),
            width: 50,
            height: 50,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                image: DecorationImage(
                    image: AssetImage(list[i]['img'] ?? "img/background.jpg"),
                    fit: BoxFit.cover),
              ),
            ),
          ),
      ],
    );
  }
}
