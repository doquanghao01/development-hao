import 'package:flutter/material.dart';

import 'widgets/detail_body.dart';

class DetailPage extends StatelessWidget {
  const DetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: const Color(0xFFc5e5f3),
        child: const DetailBody(),
      ),
    );
  }
}
