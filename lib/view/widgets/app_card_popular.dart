import 'package:development/route/screen_bindings.dart';
import 'package:development/view/detail/detail_page.dart';
import 'package:development/view/widgets/text_content.dart';
import 'package:development/view/widgets/text_title.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppCardPopular extends StatelessWidget {
  const AppCardPopular({
    Key? key,
    required this.list,
    required this.i,
  }) : super(key: key);

  final List list;
  final int i;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.to(
            arguments: {'tile': list[i]},
            () => const DetailPage(),
            binding: ScreenBindings());
      },
      child: Container(
        padding: const EdgeInsets.only(left: 20, top: 20),
        width: Get.width - 20,
        margin: const EdgeInsets.only(right: 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            color:
                i.isEven ? const Color(0xFF69c5df) : const Color(0xFF9294cc)),
        child: Column(
          children: [
            TextTitle(
              text: list[i]['title'],
              color: Colors.white,
              size: 30,
            ),
            const SizedBox(height: 10),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              child: TextContent(
                text: list[i]["text"],
                color: const Color(0xFFb8eefc),
                size: 20,
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            const Divider(
              thickness: 1.0,
            ),
            Row(
              children: [
                for (int i = 0; i < 4; i++)
                  SizedBox(
                    width: 50,
                    height: 50,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                        image: DecorationImage(
                            image: AssetImage(list[i]['img']),
                            fit: BoxFit.cover),
                      ),
                    ),
                  )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
