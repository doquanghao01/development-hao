import 'package:flutter/material.dart';

class TextContent extends StatelessWidget {
  const TextContent(
      {Key? key, required this.text, this.size = 32, this.color = Colors.white})
      : super(key: key);
  final String text;
  final double size;
  final Color color;
  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(fontSize: size, color: color),
    );
  }
}
