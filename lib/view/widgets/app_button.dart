import 'package:flutter/material.dart';

class AppButton extends StatelessWidget {
  const AppButton(
      {Key? key,
      this.height = 70,
      this.width = 200,
      required this.text,
      this.colorText = Colors.white,
      this.colorButton = const Color(0xFffbc33e),
      required this.press,
      this.padding = const EdgeInsets.all(10)})
      : super(key: key);
  final double height;
  final double width;
  final String text;
  final Color colorText;
  final Color colorButton;
  final VoidCallback press;
  final EdgeInsets padding;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: MaterialButton(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(40)),
        ),
        padding: padding,
        color: colorButton,
        minWidth: double.infinity,
        onPressed: press,
        child: Text(
          text,
          style: TextStyle(color: colorText),
        ),
      ),
    );
  }
}
