import 'package:development/controllers/content_controllers.dart';
import 'package:get/get.dart';

import '../controllers/detail_controller.dart';

class ScreenBindings implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ContentController());
    Get.lazyPut(() => DetailController());
  }
}
