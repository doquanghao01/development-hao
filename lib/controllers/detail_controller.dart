import 'package:development/models/data/list_json.dart';
import 'package:get/get.dart';

class DetailController extends GetxController {
  List listImg = [].obs;
  @override
  void onInit() {
    readData();
    super.onInit();
  }

  readData() async {
    listImg = await ListJson.readDataImg();
    update();
  }
}
