import 'package:development/models/data/list_json.dart';
import 'package:get/get.dart';

class ContentController extends GetxController {
  List listDetail = [].obs;
  List listRecent = [].obs;
  @override
  void onInit() {
    readData();
    super.onInit();
  }

  readData() async {
    listRecent = await ListJson.readDataRecent();
    listDetail = await ListJson.readDataDetail();
    update();
  }
}
