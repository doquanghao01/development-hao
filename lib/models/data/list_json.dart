import 'dart:convert';

import 'package:flutter/services.dart';

class ListJson {
  static Future<List> readDataDetail() async {
    return json.decode(await rootBundle.loadString('json/detail.json'));
  }

  static Future<List> readDataRecent() async {
    return json.decode(await rootBundle.loadString('json/recent.json'));
  }

  static Future<List> readDataImg() async {
    return json.decode(await rootBundle.loadString('json/img.json'));
  }
}
